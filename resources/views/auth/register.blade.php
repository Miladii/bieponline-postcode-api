@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registreer</div>
                <div class="panel-body">
                    <form class="form-horizontal address"role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        
    
                         <div class="form-group{{ $errors->has('voornaam') ? ' has-error' : '' }}">
                            <label for="voornaam" class="col-md-4 control-label">Voornaam</label>

                            <div class="col-md-6">
                                <input id="voornaam" type="text" class="form-control" name="voornaam" value="{{ old('') }}" required autofocus>

                                @if ($errors->has('voornaam'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('voornaam') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       

                        <div class="form-group{{ $errors->has('achternaam') ? ' has-error' : '' }}">
                            <label for="achternaam" class="col-md-4 control-label">Achternaam</label>

                            <div class="col-md-6">
                                <input id="achternaam" type="text" class="form-control" name="achternaam" value="{{ old('') }}" required autofocus>

                                @if ($errors->has('achternaam'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('achternaam') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><br>


                        <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                            <label for="postcode" class="col-md-4 control-label">Postcode</label>

                            <div class="col-md-6">
                                <input id="postcode" type="text" class="form-control postcode" name="postcode" value="{{ old('') }}" required autofocus>

                                @if ($errors->has('postcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       

                       
                        <div class="form-group{{ $errors->has('huisnummer') ? ' has-error' : '' }}">
                            <label for="huisnummer" class="col-md-4 control-label">Huisnummer</label>

                            <div class="col-md-6">
                              <input id="huisnummer" type="text" class="form-control streetnumber" name="huisnummer" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('huisnummer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('huisnummer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
            

                      <div class="form-group{{ $errors->has('straatnaam') ? ' has-error' : '' }}">
                            <label for="straatnaam" class="col-md-4 control-label">Straatnaam</label>

                            <div class="col-md-6">
                                <input id="straatnaam" type="text" class="form-control street" readonly="straatnaam" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('straatnaam'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('straatnaam') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
            
                        
                        <div class="form-group{{ $errors->has('woonplaats') ? ' has-error' : '' }}">
                            <label for="woonplaats" class="col-md-4 control-label">Woonplaats</label>

                            <div class="col-md-6">
                                <input id="woonplaats" type="text" class="form-control city" readonly="woonplaats" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('woonplaats'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('woonplaats') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group{{ $errors->has('telefoonummer') ? ' has-error' : '' }}">
                            <label for="telefoonummer" class="col-md-4 control-label">Telefoonummer</label>

                            <div class="col-md-6">
                                <input id="telefoonummer" type="text" class="form-control" name="telefoonummer" value="{{ old('') }}" required autofocus>

                                @if ($errors->has('telefoonummer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telefoonummer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><br>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mailadres</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Wachtwoord</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Bevestig het wachtwoord</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registreer
                                </button>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
      <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="js/autocomplete.js"></script>
    <script>
    var pro6pp_auth_key = "nfRt7sDVUTxKH9co";
    $(document).ready(function() {
        $(".address").applyAutocomplete();
    });
    </script>
@endsection

